from django.contrib.auth.models import User
from rest_framework.viewsets import ModelViewSet, ViewSet
from rest_framework.authtoken.serializers import AuthTokenSerializer
from rest_framework.authtoken.views import ObtainAuthToken

from users.serializers import UserSerializer


class UserViewSet(ModelViewSet):
    """
    create: Создание нового пользователя
    """
    queryset = User.objects.all()
    serializer_class = UserSerializer


class AuthViewSet(ViewSet):
    """
    create: Авторизация пользователя с возвратом токена
    """
    serializer_class = AuthTokenSerializer

    def create(self):
        return ObtainAuthToken().post(self.request)
