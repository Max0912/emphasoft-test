from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status
from rest_framework.test import APITestCase


class UserTests(APITestCase):
    def setUp(self):
        user = User.objects.create(username='test')
        user.set_password('test')
        user.save()

        self.user_create_instance = {'username': 'inst', 'password': 'test'}
        self.user_auth_instance = {'username': 'test', 'password': 'test'}
        self.user = User.objects.create(username='user', password='user')
        self.user_instance = {'username': 'another_name',  'password': 'user'}

    def test_create_user(self):
        """
        Test POST return 'id', 'username', 'token' fields
        """
        url = reverse('users-list')
        response = self.client\
            .post(url, self.user_create_instance, format='json')

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        self.assertTrue('id' in response.data)
        self.assertTrue('username' in response.data)
        self.assertTrue('token' in response.data)
        self.assertFalse('password' in response.data)

    def test_auth_user(self):
        """
        Test POST return 'token' field
        """
        response = self.client\
            .post('/api-token-auth/', self.user_auth_instance, format='json')

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertTrue('token' in response.data)

    def test_can_read_users_list(self):
        response = self.client.get(reverse('users-list'))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_read_user_detail(self):
        url = reverse('users-detail', args=(self.user.id,))
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_can_update_user(self):
        url = reverse('users-detail', args=(self.user.id,))
        response = self.client.put(url, self.user_instance)
        self.assertEqual(response.status_code, status.HTTP_200_OK)

    def test_delete_user(self):
        url = reverse('users-detail', args=(self.user.id,))
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
